#!/usr/bin/env bash

set -eux

rm -vf configure~
rm -vf configure
autoreconf --verbose --install --force
./configure
# sed -i 's/^all: all-am/all: all-am setuid/g' Makefile
# sed -i 's/^install-am: all-am/install-am: all-am setuid/g' Makefile
